# Rutzkat

## Setup the project

Run `yarn` to install the dependencies.

Create a `.env` file, and fill it with your local config (or set the
environment variables by hand).

### Environment variables

- *PORT* (=5000) : Port the server will listen to

- *FORCE_HTTPS* (=false) : FOrce all traffic to use HTTPS

- *SOURCE_URL*: URL for the server to contact to retrieve the books data

### .env example for dev

```
SOURCE_URL=https://localhost:5000/api
```

## Start the server

### dev

Create a pharo image, import the repository with Iceberg (subdirectory `server`).

Run the Gofer commands in `setup-vm.st` in the Playground.

Then run `RutzkatServer config: RutzkatConfig dev` in the
Playground. The server will start on `http://localhost:8080`.

## Start the frontend dev server

Polymer, bs and webpack dev servers

```
yarn dev
```

## Build the app for production

```sh
docker-compose build
```
