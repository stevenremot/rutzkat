| repository |

repository := (IceRepositoryCreator new
	location: './' asFileReference asAbsolute ;
	subdirectory: 'server';
	createRepository).

(repository packageNamed: #Rutzkat) loadLatest.

RutzkatProject installDependencies.

Smalltalk snapshot: true andQuit: true