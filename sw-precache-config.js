module.exports = {
  staticFileGlobs: [
    '/index.html',
    '/manifest.json',
    '/bower_components/webcomponentsjs/webcomponents-lite.js',
    'assets/*',
    'client/build/*',
  ],
  navigateFallback: 'index.html',
};
