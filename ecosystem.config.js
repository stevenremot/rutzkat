module.exports = {
  apps : [
    {
      name: 'bsb',
      script: 'npm',
      args: 'run start',
      cwd: './client/',
    },

    {
      name: 'webpack',
      script: 'npm',
      args: 'run webpack',
      cwd: './client',
    },

    {
      name: 'polymer',
      script: 'npm',
      args: 'run dev:server:start',
      cwd: '.',
    },
  ],
};
