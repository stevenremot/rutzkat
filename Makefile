build/pharo:
	mkdir -p build
	cd build; \
	curl https://get.pharo.org/64/stable+vm | bash

build/Deploy.image: build/pharo
	cd build; \
	VM_SCRIPT=../setup-vm.st bash ../make-image.sh

image: build/Deploy.image

run-image: image
	cd build; \
	PORT=8082 STATIC_FILES_DIR=./default/ ./pharo Deploy.image ../start-server.st

build/default:
	SOURCE_URL=/api yarn build

.PHONY: client
client: build/default

docker: client image
	docker build . -t rutzkat

docker-run-local: docker
	docker run -p 8082:8082 -i -e PORT=8082 rutzkat

.PHONY: deploy
deploy: docker
	heroku container:login
	heroku container:push web --app rutzkat

clean: clean-client
	rm -rf build

clean-image:
	rm -f build/Deploy.*

clean-client:
	rm -rf build/default client/build
	cd client && yarn clean
