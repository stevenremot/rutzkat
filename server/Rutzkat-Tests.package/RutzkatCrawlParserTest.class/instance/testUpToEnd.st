tests
testUpToEnd
	| parser |
	parser := RutzkatCrawlParser
		on:
			'[{"title":"7th Garden","volume":5,"date":"2018-05-02T00:00:00.000Z"},{"title":"Beyblade - Burst","volume":null,"date":"2018-05-02T00:00:00.000Z"}]'
				readStream.
	self
		assert: parser upToEnd
		equals:
			{(RutzkatBookModel new
				title: '7th Garden';
				volume: 5;
				editor: '';
				isbn: '7th Garden 5';
				date: (Date newDay: 2 month: 5 year: 2018)).
			(RutzkatBookModel new
				title: 'Beyblade - Burst';
				editor: '';
				isbn: 'Beyblade - Burst nil';
				date: (Date newDay: 2 month: 5 year: 2018))}