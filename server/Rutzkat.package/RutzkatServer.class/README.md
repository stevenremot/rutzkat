I am the main entry point of rutzkat.

I redirect all calls to /api to a RutzkatApiServer, and the rest is resolved statically.

	RutzkatServer config: RutzkatConfig fromEnv