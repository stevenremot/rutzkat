public
handleRequest: request
	request uri firstPathSegment = #api
		ifTrue: [ ^ self apiServer handleRequest: request ]
		ifFalse: [ ^ self staticServer handleRequest: request ]