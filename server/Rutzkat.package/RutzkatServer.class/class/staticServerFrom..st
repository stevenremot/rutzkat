public
staticServerFrom: config
	^ config useDevServer
		ifTrue: [ RutzkatDevServer config: config ]
		ifFalse: [ ZnStaticFileServerDelegate new
				directory: config staticFilesDir asFileReference ]