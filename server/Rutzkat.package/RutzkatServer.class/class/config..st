public
config: config
	| delegate |
	delegate := self new
		apiServer: (RutzkatApiServer config: config);
		staticServer: (self staticServerFrom: config).
	config forceHttps
		ifTrue: [ delegate := RutzkatHerokuSSLRedirector
				delegate: delegate ].
	ZnServer startDefaultOn: config port.
	ZnServer default
		logToTranscript;
		delegate: delegate