public
redirectToHttps: request
	| hostHeader url |
	url := request url copy scheme: 'https'.
	hostHeader := (request headers at: 'Host')
		splitOn: ':'.
	url host: (hostHeader at: 1).
	hostHeader size > 1
		ifTrue: [ url port: (hostHeader at: 2) asInteger ].
	^ ZnResponse redirect: url