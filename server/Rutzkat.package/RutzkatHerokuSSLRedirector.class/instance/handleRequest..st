public
handleRequest: request
	| scheme |
	scheme := request headers at: 'X-Forwarded-Proto' ifAbsent: request url scheme.
	
	scheme = 'https'
		ifTrue: [ ^ self delegate handleRequest: request ]
		ifFalse: [ ^ self redirectToHttps: request ]
	
	