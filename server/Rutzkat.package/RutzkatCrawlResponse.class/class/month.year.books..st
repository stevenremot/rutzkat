as yet unclassified
month: aMonth year: aYear books: bookList
	^ self new
		month: aMonth;
		year: aYear;
		books: bookList