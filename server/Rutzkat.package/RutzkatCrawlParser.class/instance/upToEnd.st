accessing
upToEnd
	^ (self jsonParser nextListAs: RutzkatBookModel)
		do: [ :each | 
			each
				isbn: each title, ' ', each volume asString;
				editor: '' ]