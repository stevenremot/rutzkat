accessing
jsonParser
	jsonParser
		ifNil: [ jsonParser := self jsonParserClass on: readStream.
			jsonParser
				for: Date
				customDo: [ :mapping | mapping decoder: [ :string | Date fromString: string ] ].
			jsonParser
				for: Number
				customDo: [ :mapping | mapping decoder: [ :string | string ifNotNil: [ string asNumber ] ] ].
			jsonParser
				for: RutzkatBookModel
				do: [ :mapping | 
					mapping mapInstVar: #title.
					(mapping mapInstVar: #volume) valueSchema: Number.
					(mapping mapInstVar: #date) valueSchema: Date ] ].
	^ jsonParser