converting
neoJsonOn: json
	json
		writeMap:
			{('title' -> title).
			('isbn' -> isbn).
			('volume' -> volume).
			('editor' -> editor).
			('date' -> date asDateAndTime printString)} asDictionary