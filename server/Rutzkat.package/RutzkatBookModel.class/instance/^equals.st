comparing
= aBookModel
	^ self species = aBookModel species
		and: [ self editor = aBookModel editor
				and: [ self date = aBookModel date
						and: [ self isbn = aBookModel isbn
								and: [ self title = aBookModel title and: self volume = aBookModel volume ] ] ] ]