network
requestForMonth: aMonth year: aYear
	^ ZnClient new
		url: self baseUrl , '/manga-sanctuary?month=', aMonth asString, '&year=', aYear asString;
		get