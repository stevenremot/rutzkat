network
fetchMonth: aMonth year: aYear
	^ RutzkatCrawlResponse
		month: aMonth
		year: aYear
		books:
			(self parserClass
				on:
					(self requestForMonth: aMonth year: aYear)
						readStream) upToEnd