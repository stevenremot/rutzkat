instance creation
fromEnv
	| env |
	env := OSEnvironment default.
	^ self new
		port: (env at: 'PORT' ifAbsent: [ 80 ]) asInteger;
		forceHttps: (env at: 'FORCE_HTTPS' ifAbsent: [ 'false' ]) = 'true';
		useDevServer: false;
		staticFilesDir: (env at: 'STATIC_FILES_DIR');
		crawlerUrl: (env at: 'CRAWLER_URL')