instance creation
dev
	^ self new
		port: 8080;
		forceHttps: false;
		useDevServer: true;
		devServerUrl: 'http://localhost:8081';
		crawlerUrl: 'http://localhost:3000'