I represent the configuration of the rutzkat server

Get a config based on environment variables:

	RutzkatConfig fromEnv
	
Get a development config:

	RutzkatConfig dev