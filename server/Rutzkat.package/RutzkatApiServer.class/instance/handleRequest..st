public
handleRequest: request
	| response |
	response := request uri lastPathSegment = 'fetch'
		ifTrue: [ self handleFetchRequest: request ]
		ifFalse: [ ZnResponse notFound: request uri ].
	response headers
		at: 'Access-Control-Allow-Origin'
		put: '*'.
	^ response