public
handleFetchRequest: request
	| month year response |
	month := request uri queryAt: 'month'.
	year := request uri queryAt: 'year'.
	(RutzkatFetchingEvent
		month: month
		year: year) emit.
	response := (RutzkatCrawlFetcher baseUrl: config crawlerUrl)
		fetchMonth: month
		year: year.
	^ ZnResponse
		ok:
			(ZnEntity
				with: (NeoJSONWriter toString: response books)
				type: ZnMimeType applicationJson)