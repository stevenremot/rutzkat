public
handleRequest: request
	| newRequest |
	newRequest := request copy.
	newRequest uri
		scheme: devServerUrl scheme;
		host: devServerUrl host;
		port: devServerUrl port.
	^ ZnClient new
		request: newRequest;
		execute;
		response