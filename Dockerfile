FROM ubuntu:16.04
ENV DIR=/var/local/web-app
ENV STATIC_FILES_DIR=$DIR/static/
ENV PHARO_DIR=/usr/local/pharo

RUN mkdir -p $DIR
WORKDIR $DIR

COPY make-image.sh start-server.st setup-vm.st ./
RUN mkdir server/ .git/
COPY server/ server/
COPY .git/ .git/

RUN mkdir -p $PHARO_DIR && cd $PHARO_DIR && \
    sh $DIR/server/docker-scripts/install-pharo.sh && \
    cd $DIR && \
    VM_SCRIPT=./setup-vm.st sh make-image.sh

CMD ./pharo Deploy.image start-server.st
