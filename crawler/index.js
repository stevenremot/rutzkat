const puppeteer = require('puppeteer');
const Hapi = require('hapi');

// /////////////////////////////////////////////////////////////////////////////
// Puppeteer

const options = process.env.PUPPETEER_EXECUTABLE_PATH
  ? { executablePath: process.env.PUPPETEER_EXECUTABLE_PATH }
  : {};

const browserPromise = puppeteer.launch(options);

function getBrowser() {
  return browserPromise;
}

// /////////////////////////////////////////////////////////////////////////////
// Manga sanctuary

const tableSelector = 'table.collection';

const dateMatcher = /^(.+)\s([\d]{2})\s(.+)\s([\d]{4})$/;
const titleMatcher = /^(.+) T\.([\d]+).+$/;

function createDate(month, year, text) {
  const match = text.match(dateMatcher);
  return match ? new Date(year, month - 1, match[2]) : null;
}

function extractInfo(elt, date) {
  const match = elt
    .replace(/\n/g, '')
    .trim()
    .match(titleMatcher);

  return {
    title: match[1],
    volume: Number.parseInt(match[2], 10),
    date,
  };
}

async function fetchMangaSanctuary({ month, year }) {
  const browser = await getBrowser();
  const page = await browser.newPage();
  try {
    await page.goto(
      `https://www.manga-sanctuary.com/planning/?support=4&annee=${year}&mois=${month}&pays=77&affichage=tableau`,
    );
    await page.waitForSelector(tableSelector);
    return (await page.evaluate(selector => {
      /* eslint-env browser */
      const table = document.querySelector(selector);
      return Array.from(table.querySelectorAll('tr:not(.titre)')).map(
        row =>
          row.childNodes.length === 1
            ? { type: 'heading', content: row.textContent }
            : { type: 'item', content: row.querySelector('td').textContent },
      );
      /* eslint-env node */
    }, tableSelector))
      .map(({ type, content }) => ({
        type,
        content: content.replace(/\n/g, '').trim(),
      }))
      .reduce(
        ({ date, entries }, row) => {
          if (row.type === 'heading') {
            return { date: createDate(month, year, row.content), entries };
          }

          return {
            date,
            entries: [...entries, extractInfo(row.content, date)],
          };
        },
        { date: null, entries: [] },
      ).entries;
  } catch (e) {
    throw e;
  } finally {
    page.close();
  }
}

// /////////////////////////////////////////////////////////////////////////////
// Server

const server = Hapi.Server({
  host: process.env.HOST,
  port: process.env.PORT,
  debug: { request: ['*'] },
});

server.route({
  method: 'GET',
  path: '/manga-sanctuary',
  async handler(request, h) {
    const month = Number.parseInt(request.query.month, 10);
    const year = Number.parseInt(request.query.year, 10);
    request.log(['info'], `Asking manga sanctuary for ${month}/${year}`);
    const response = h.response(
      JSON.stringify(await fetchMangaSanctuary({ month, year })),
    );
    response.type('application/json');
    return response;
  },
});

async function main() {
  await server.start();
  console.log(`Server running at ${server.info.uri}`);
}

process.on('unhandledRejection', err => {
  console.log(err);
  process.exit(1);
});

main();
