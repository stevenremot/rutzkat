type requestId = (int, int);

type model = {
  isbn: string,
  title: string,
  volume: option(int),
  date: Js.Date.t,
  editor: string,
  requestId: option(requestId)
};

let sourceUrl = Config.sourceUrl;

module Decode = {
  let requestId = Json.Decode.(pair(int, int));
  let book = (book: Js.Json.t) : model =>
    Json.Decode.{
      isbn: book |> field("isbn", string),
      title: book |> field("title", string),
      volume: book |> optional(field("volume", int)),
      date: book |> field("date", string) |> Js.Date.fromString,
      editor: book |> field("editor", string),
      requestId: book |> optional(field("requestId", requestId))
    };
  let books = books => books |> Json.Decode.list(book);
};

module Encode = {
  let requestId = Json.Encode.(pair(int, int));
  let book = (book: model) =>
    Json.Encode.(
      object_([
        ("isbn", string(book.isbn)),
        ("title", string(book.title)),
        ("volume", nullable(int, book.volume)),
        ("date", string(Js.Date.toISOString(book.date))),
        ("editor", string(book.editor)),
        ("requestId", nullable(requestId, book.requestId))
      ])
    );
  let books = Json.Encode.list(book);
};

module RequestType = {
  type t = requestId;
  let compare = ((m1, y1): t, (m2, y2): t) => {
    let yearComparison = compare(y1, y2);
    if (yearComparison == 0) {
      compare(m1, m2);
    } else {
      yearComparison;
    };
  };
};

module RequestsSet = Set.Make(RequestType);

module Model = {
  /**
   * Return a set of request ids found in the books
   */
  let getRequestIds = (books: list(model)) =>
    List.fold_left(
      (requests: RequestsSet.t, book: model) =>
        switch book.requestId {
        | Some(req) => RequestsSet.add(req, requests)
        | None => requests
        },
      RequestsSet.of_list([]),
      books
    );
  /**
   * Return the current date's month and year
   */
  let getCurrentMonthYear = () => {
    let baseDate = Js.Date.make();
    let month = int_of_float(Js.Date.getMonth(baseDate));
    let year = int_of_float(Js.Date.getFullYear(baseDate));
    (month, year);
  };
  /**
   * Utility function to return either book's request id, either soem placeholder
   */
  let getBookMonthYear = (book: model) =>
    switch book.requestId {
    | Some(req) => req
    | None => (0, 0)
    };
  /**
   * Remove all books whose month and year are before now
   */
  let filterOutdatedBooks = (books: list(model)) => {
    let currentMonthYear = getCurrentMonthYear();
    let isUpToDate = (book: model) =>
      RequestType.compare(getBookMonthYear(book), currentMonthYear) >= 0;
    List.filter(isUpToDate, books);
  };
  /**
   * Remove all books that are associated with the given request
   */
  let removeBooksWithRequests = (requests: RequestsSet.t, books: list(model)) =>
    books
    |> List.filter(book => ! RequestsSet.mem(getBookMonthYear(book), requests));
  /**
   * Remove from the current books the one with the same requests,
   * and replace them with the new ones
   */
  let changeBooks = (currentBooks: list(model), newBooks: list(model)) =>
    currentBooks
    |> removeBooksWithRequests(getRequestIds(newBooks))
    |> List.append(newBooks);
  /**
   * Return all books whose names contains the filter
   */
  let filterByName = (filter: string) =>
    List.filter((book: model) =>
      Js.String.includes(
        String.lowercase(filter),
        String.lowercase(book.title)
      )
    );
};

module Storage = {
  let key = "rutzkat-books-list";
  let storage = Dom.Storage.localStorage;
  let getBooks = () =>
    switch (Dom.Storage.getItem(key, storage)) {
    | Some(rawValue) => rawValue |> Json.parseOrRaise |> Decode.books
    | None => []
    };
  let getBooksByName = (name: string) =>
    getBooks() |> List.filter(book => book.title == name);
  let saveBooks = (books: list(model)) =>
    Dom.Storage.setItem(key, books |> Encode.books |> Json.stringify, storage);
  let changeBooks = (books: list(model)) =>
    Model.changeBooks(getBooks(), books) |> saveBooks;
};

module Network = {
  let formatMonthUrl = (month: int, year: int) => {j| $sourceUrl/fetch?year=$year&month=$month |j};
  let fetchMonth = (month: int, year: int) =>
    Js.Promise.(
      Fetch.fetch(formatMonthUrl(month + 1, year))
      |> then_(Fetch.Response.json)
      |> then_(json => Decode.books(json) |> resolve)
      |> then_(books =>
           books
           |> List.map(book => {...book, requestId: Some((month, year))})
           |> resolve
         )
    );
  let fetchMonths = (months: list((int, int))) =>
    Js.Promise.(
      months
      |> List.map(((month, year)) => fetchMonth(month, year))
      |> Array.of_list
      |> all
      |> then_(results => results |> Array.to_list |> List.flatten |> resolve)
    );
};

module Sync = {
  /**
   * Fetch the months and replace local storage books of the
   * same months with the new ones
   */
  let synchronizeMonths = (months: list((int, int))) =>
    Js.Promise.(
      Network.fetchMonths(months)
      |> then_(books => books |> Storage.changeBooks |> resolve)
    );
};

module State = {
  type state = {
    monthsNumber: int,
    isLoading: bool
  };
  let initialState = () => {
    monthsNumber:
      max(
        2,
        Storage.getBooks()
        |> Model.getRequestIds
        |> RequestsSet.elements
        |> List.length
      ),
    isLoading: false
  };
  let startLoading = (state: state) => {...state, isLoading: true};
  let stopLoading = (state: state) => {...state, isLoading: false};
  let addMonth = (state: state) => {
    ...state,
    monthsNumber: state.monthsNumber + 1
  };
  let rec normalizeMonthYearPair = ((month, year)) =>
    if (month <= 11) {
      (month, year);
    } else {
      normalizeMonthYearPair((month - 12, year + 1));
    };
  let getStateMonths = (oldState: state, newState: state) => {
    let (baseMonth, baseYear) = Model.getCurrentMonthYear();
    let rec getPairs = (start, end_) =>
      if (end_ <= start) {
        [];
      } else {
        let pair = (baseMonth + start, baseYear) |> normalizeMonthYearPair;
        [pair, ...getPairs(start + 1, end_)];
      };
    getPairs(oldState.monthsNumber, newState.monthsNumber);
  };
  /**
   * Send the requests necesaries to jump from oldState to newState
   */
  let sendRequests = (oldState, newState) =>
    getStateMonths(oldState, newState) |> Sync.synchronizeMonths;
  let sendInitialRequests = sendRequests({monthsNumber: 0, isLoading: false});
};
