[@bs.val]
external decodeUriComponent : string => string = "decodeURIComponent";

type page =
  | List
  | View(string);

let log = text => {
  Js.log(text);
  Js.Promise.resolve();
};

type state = {
  page,
  books: Book.State.state,
  favorites: Favorites.State.state
};

type action =
  | SetPage(page)
  | ToggleFavorite(string)
  | StopLoading
  | AddMonth;

let component = ReasonReact.reducerComponent("App");

let make = _children => {
  ...component,
  initialState: () => {
    page: List,
    books: Book.State.initialState(),
    favorites: Favorites.State.initialState()
  },
  reducer: (action, state) =>
    switch action {
    | SetPage(page) => ReasonReact.Update({...state, page})
    | StopLoading =>
      ReasonReact.Update({
        ...state,
        books: Book.State.stopLoading(state.books)
      })
    | AddMonth =>
      if (! state.books.isLoading) {
        let newBookState =
          state.books |> Book.State.addMonth |> Book.State.startLoading;
        ReasonReact.UpdateWithSideEffects(
          {...state, books: newBookState},
          (
            self =>
              Js.Promise.(
                Book.State.sendRequests(state.books, newBookState)
                |> then_(() => self.send(StopLoading) |> resolve)
              )
              |> ignore
          )
        );
      } else {
        ReasonReact.NoUpdate;
      }
    | ToggleFavorite(title) =>
      let newFavState = Favorites.Model.toggle(title, state.favorites);
      ReasonReact.UpdateWithSideEffects(
        {...state, favorites: newFavState},
        (_self => Favorites.Storage.setFavorites(newFavState))
      );
    },
  subscriptions: self => [
    Sub(
      () =>
        ReasonReact.Router.watchUrl(url =>
          switch url.path {
          | ["list"] => self.send(SetPage(List))
          | ["book", id] => self.send(SetPage(View(decodeUriComponent(id))))
          | _ => self.send(SetPage(List))
          }
        ),
      ReasonReact.Router.unwatchUrl
    )
  ],
  didMount: self =>
    ReasonReact.UpdateWithSideEffects(
      {...self.state, books: Book.State.startLoading(self.state.books)},
      self =>
        Js.Promise.(
          Book.State.sendInitialRequests(self.state.books)
          |> then_(() => self.send(StopLoading) |> resolve)
        )
        |> ignore
    ),
  render: self =>
    <div>
      <Router.Page
        pagePredicate=(page => page == List)
        currentPage=self.state.page
        frozen=true>
        <Pages.ListPage
          isLoading=self.state.books.isLoading
          onApproachEnd=(() => self.send(AddMonth))
          favorites=self.state.favorites
        />
      </Router.Page>
      <Router.Page
        pagePredicate=(
          page =>
            switch page {
            | View(_) => true
            | _ => false
            }
        )
        currentPage=self.state.page>
        (
          switch self.state.page {
          | View(bookTitle) =>
            <Pages.ViewPage
              bookTitle
              favorites=self.state.favorites
              onToggleFavorite=(() => self.send(ToggleFavorite(bookTitle)))
            />
          | _ =>
            <Pages.ViewPage
              bookTitle=""
              favorites=self.state.favorites
              onToggleFavorite=(() => self.send(ToggleFavorite("")))
            />
          }
        )
      </Router.Page>
    </div>
};
