import React from "react";

export class ViewPage extends React.Component {
  constructor(props) {
    super(props);

    this.innerRef = null;
		this.handleFavoriteToggle = this.handleFavoriteToggle.bind(this);
  }

  handleRef(ref) {
    this.innerRef = ref;
  }

  componentDidMount() {
    this.innerRef.addEventListener("favorite-toggle", this.handleFavoriteToggle);
  }

  componentWillUnmount() {
    this.innerRef.removeEventListener("favorite-toggle", this.handleFavoriteToggle);
  }

  handleFavoriteToggle(event) {
    this.props.onFavoriteToggle();
  }

  render() {
    const { book, books, isFavorite } = this.props;

    const optionalProps = {};

    if (isFavorite) {
      optionalProps.isfavorite = "true";
    }

    return (
      <rutzkat-book-view-page
        ref={this.handleRef.bind(this)}
        book={book}
        books={JSON.stringify(books)}
        {...optionalProps}
      />
    );
  }
}
