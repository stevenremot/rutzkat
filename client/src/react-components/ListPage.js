import React from "react";

export class ListPage extends React.Component {
  constructor(props) {
    super(props);

    this.innerRef = null;
    this.handleItemClick = this.handleItemClick.bind(this);
    this.handleApproachEnd = this.handleApproachEnd.bind(this);
    this.handleFilterChange = this.handleFilterChange.bind(this);
  }

  handleRef(ref) {
    this.innerRef = ref;
  }

  componentDidMount() {
    this.innerRef.addEventListener("item-click", this.handleItemClick);
    this.innerRef.addEventListener("approach-end", this.handleApproachEnd);
    this.innerRef.addEventListener("filter-changed", this.handleFilterChange);
  }

  componentWillUnmount() {
    this.innerRef.removeEventListener("item-click", this.handleItemClick);
    this.innerRef.removeEventListener("approach-end", this.handleApproachEnd);
    this.innerRef.removeEventListener(
      "filter-changed",
      this.handleFilterChange
    );
  }

  handleItemClick(event) {
    this.props.onItemClick(event.detail.item);
  }

  handleApproachEnd() {
    this.props.onApproachEnd();
  }

  handleFilterChange(event) {
    this.props.onFilterChange(event.detail.value);
  }

  render() {
    const { books, isLoading, filter, favorites } = this.props;
    return (
      <rutzkat-book-list-page
        ref={this.handleRef.bind(this)}
        books={JSON.stringify(books)}
        is-loading={isLoading ? "is-loading" : null}
        filter={filter}
        favorites={JSON.stringify(favorites)}
      />
    );
  }
}
