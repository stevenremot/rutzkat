type book = {
  isbn: string,
  title: string,
  volume: option(int),
  date: Js.Date.t,
  editor: string,
  requestId: string
};
