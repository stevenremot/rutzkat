[@bs.module "./react-components/StarButton"]
external starButton : ReasonReact.reactClass = "StarButton";

module Page = {
  let component = ReasonReact.statelessComponent("UiPage");
  let make = (~title: string, children) => {
    ...component,
    render: _self =>
      ReasonReact.createDomElement(
        "rutzkat-page",
        ~props={"title": title},
        children
      )
  };
};

[@bs.val] external goBack : unit => unit = "history.back";

module BackButton = {
  let component = ReasonReact.statelessComponent("UiBackButton");
  let make = _children => {
    ...component,
    render: _self =>
      ReasonReact.createDomElement(
        "rutzkat-back-button",
        ~props={"slot": "left-button", "onClick": goBack},
        [||]
      )
  };
};

module StarButton = {
  let component = ReasonReact.statelessComponent("UiStarButton");
  let make = (~isActive: bool, ~onClick: unit => unit, _children) => {
    ...component,
    render: _self =>
      if (isActive) {
        ReasonReact.createDomElement(
          "rutzkat-star-button",
          ~props={
            "slot": "action-buttons",
            "onClick": onClick,
            "is-active": "true"
          },
          [||]
        );
      } else {
        ReasonReact.createDomElement(
          "rutzkat-star-button",
          ~props={"slot": "action-buttons", "onClick": onClick},
          [||]
        );
      }
  };
};

module BookItemList = {
  let component = ReasonReact.statelessComponent("UiBookItemList");
  let make = (~books: list(Book.model), _children) => {
    ...component,
    render: _self =>
      ReasonReact.createDomElement(
        "book-item-list",
        ~props={"items": Book.Encode.books(books) |> Json.stringify},
        [||]
      )
  };
};
