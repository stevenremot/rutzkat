type model = list(string);

module Decode = {
  let favorites = Json.Decode.list(Json.Decode.string);
};

module Encode = {
  let favorites = Json.Encode.list(Json.Encode.string);
};

module Storage = {
  let key = "rutzkat-favorite-titles";
  let getFavorites = () =>
    switch (Dom.Storage.getItem(key, Dom.Storage.localStorage)) {
    | Some(json) => json |> Json.parseOrRaise |> Decode.favorites
    | None => []
    };
  let setFavorites = favorites =>
    Dom.Storage.setItem(
      key,
      Json.stringify(Encode.favorites(favorites)),
      Dom.Storage.localStorage
    );
};

module Model = {
  let isFavorite = (title: string) => List.mem(title);
  let add = (title: string) => List.append([title]);
  let remove = (title: string) => List.filter(fav => fav != title);
  let toggle = (title: string, favorites: model) =>
    if (isFavorite(title, favorites)) {
      remove(title, favorites);
    } else {
      add(title, favorites);
    };
};

module State = {
  type state = model;
  let initialState = Storage.getFavorites;
};
