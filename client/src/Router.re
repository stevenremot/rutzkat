module Page = {
  let commonStyle =
    ReactDOMRe.Style.make(
      ~position="absolute",
      ~left="0",
      ~width="100vw",
      ~height="100vh",
      ~transition="transform 300ms, opacity 300ms",
      ()
    );
  let activeStyle = ReactDOMRe.Style.make(~opacity="1", ());
  let inactiveStyle =
    ReactDOMRe.Style.make(~opacity="0", ~transform="translateX(100vw)", ());
  let getStyle = (isActive: bool, frozen: bool) =>
    ReactDOMRe.Style.combine(
      commonStyle,
      isActive || frozen ? activeStyle : inactiveStyle
    );
  let component = ReasonReact.statelessComponent("Page");
  let make =
      (~pagePredicate: 'a => bool, ~currentPage: 'a, ~frozen=false, children) => {
    ...component,
    render: _self =>
      ReasonReact.createDomElement(
        "div",
        ~props={"style": getStyle(pagePredicate(currentPage), frozen)},
        children
      )
  };
};
