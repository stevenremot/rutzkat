[@bs.module "./react-components/ListPage"]
external listPage : ReasonReact.reactClass = "ListPage";

[@bs.module "./react-components/ViewPage"]
external viewPage : ReasonReact.reactClass = "ViewPage";

module InternalListPage = {
  type item = {. "title": string};
  let make =
      (
        ~books: list(Book.model),
        ~isLoading: bool,
        ~onItemClick: item => unit,
        ~onApproachEnd: unit => unit,
        ~filter: string,
        ~onFilterChange: string => unit,
        ~favorites: Favorites.model,
        children
      ) =>
    ReasonReact.wrapJsForReason(
      ~reactClass=listPage,
      ~props={
        "books": Book.Encode.books(books),
        "onItemClick": onItemClick,
        "isLoading": isLoading,
        "onApproachEnd": onApproachEnd,
        "filter": filter,
        "onFilterChange": onFilterChange,
        "favorites": Favorites.Encode.favorites(favorites)
      },
      children
    );
};

/* Use createDomElement */
module ListPage = {
  type state = {filter: string};
  type action =
    | SetFilter(string);
  let component = ReasonReact.reducerComponent("ListPage");
  let handleItemClick = item => {
    let book = item##title;
    ReasonReact.Router.push({j| /book/$book |j});
  };
  let make =
      (
        ~isLoading: bool,
        ~onApproachEnd: unit => unit,
        ~favorites: Favorites.model,
        _children
      ) => {
    ...component,
    initialState: () => {filter: ""},
    reducer: (action: action, _state: state) =>
      switch action {
      | SetFilter(filter) => ReasonReact.Update({filter: filter})
      },
    render: self =>
      <InternalListPage
        isLoading
        books=(
          Book.Storage.getBooks() |> Book.Model.filterByName(self.state.filter)
        )
        onItemClick=handleItemClick
        onApproachEnd
        filter=self.state.filter
        onFilterChange=(filter => self.send(SetFilter(filter)))
        favorites
      />
  };
};

module ViewPage = {
  let component = ReasonReact.statelessComponent("ViewPage");
  let make =
      (
        ~bookTitle: string,
        ~favorites: Favorites.model,
        ~onToggleFavorite: unit => unit,
        _children
      ) => {
    ...component,
    render: _self => {
      let books = Book.Storage.getBooksByName(bookTitle);
      Ui.(
        <Page title=bookTitle>
          <BackButton />
          <StarButton
            isActive=(Favorites.Model.isFavorite(bookTitle, favorites))
            onClick=onToggleFavorite
          />
          <BookItemList books />
        </Page>
      );
    }
  };
};
