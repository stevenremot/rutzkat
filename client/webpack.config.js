const webpack = require("webpack");
const path = require("path");

module.exports = {
  entry: "./src/index.bs.js",
  output: {
    path: path.join(__dirname, "build"),
    filename: "index.js"
  },
	module: {
		rules: [
			{
				test: /.js$/,
				exclude: /(node_modules)/,
				use: {
					loader: 'babel-loader',
				},
			},
		]
	},
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        SOURCE_URL: "'" + process.env.SOURCE_URL + "'"
      }
    })
  ]
};
